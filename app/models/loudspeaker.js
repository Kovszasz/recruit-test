const isIn = require('validator/lib/isIn');

module.exports = (sequelize, DataTypes) => {
  const loudspeakers = sequelize.define('loudspeakers',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      manufacturer:DataTypes.STRING,
      model:DataTypes.STRING,
      serial_no:DataTypes.STRING,
      boxtype:{
        type:DataTypes.STRING,
        validate:{
          isIn:{
            args:[['1-way','2-way','3-way']],
            msg:"Please choose from the following options: 1-way,2-way,3-way"
          }

        }
      }
    },
    {
      tableName: 'loudspeakers',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });
  return loudspeakers;
};
