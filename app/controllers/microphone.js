const express = require('express');
const connection = require('../setup/database');
const MicrophoneModel = connection.import('../models/microphone');
const router = express.Router();

// GET: Finds an entry in the Microphones table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return MicrophoneModel.findOne({ where: { id } })
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});

router.get('/', (req, res) => {
  var query = MicrophoneModel.findAll()
  return query.then(microphones => res.json(microphones))
});

// POST: Creates a new entry in Microphones
router.post('/', (req, res) => {
  var id=MicrophoneModel.max('id').then(new_id=>{
  req.body['id']=new_id+1
  var new_microphone=MicrophoneModel.create(req.body)
  return new_microphone
      .then(microphone => res.status(200).send(microphone))
      .catch((error) => res.status(500).send({ error }));
  })
});

router.delete('/:id',(req,res)=>{
  const id = parseInt(req.params.id,10);
  MicrophoneModel.destroy({ where: { id } })
  return res.status(200).send("Deleted")
});

router.put('/:id',(req,res)=>{
  MicrophoneModel.update(
      req.body,
      {where: {id:parseInt(req.params.id, 10)}}
    )
    .then(function(added) {
      res.json(added)
})
.catch((error) => res.status(500).send({ error }))

});

module.exports = router;
