'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.createTable('loudspeakers', {
                   id:{
                     type:Sequelize.INTEGER,
                     primaryKey:true,
                     allowNull: false,
                   },
                    manufacturer:Sequelize.STRING,
                    model:Sequelize.STRING,
                    serial_no:Sequelize.STRING,
                    boxtype:Sequelize.STRING,
                }, { transaction: t }),
            ])
        })
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
            ])
        })
    }
};
