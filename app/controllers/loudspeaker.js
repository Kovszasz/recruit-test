const express = require('express');
const connection = require('../setup/database');

const LoudspeakerModel = connection.import('../models/loudspeaker');
const router = express.Router();

// GET: Finds an entry in the Loudspeakers table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return LoudspeakerModel.findOne({ where: { id } })
        .then((loudspeaker) => res.status(200).send(loudspeaker))
        .catch((error) => res.status(500).send({ error }));
});

router.get('/', (req, res) => {
  var query = LoudspeakerModel.findAll()
  return query
        .then(loudspeakers => res.status(200).send(loudspeakers))
        .catch((error) => res.status(500).send({ error }));
});

// POST: Creates a new entry in Loudspeakers
router.post('/', (req, res) => {
  var id=LoudspeakerModel.max('id').then(new_id=>{
  req.body['id']=new_id+1

  var new_loud_speaker=LoudspeakerModel.create(req.body)
  return new_loud_speaker
    .then(loudspeaker => res.status(200).send(loudspeaker))
    .catch((error) => res.status(500).send({ error }));
  })
});

router.delete('/:id',(req,res)=>{
  const id = parseInt(req.params.id,10);
  LoudspeakerModel.destroy({ where: { id } })
    return res.status(200).send("Deleted")
});

router.put('/:id',(req,res,next)=>{
  LoudspeakerModel.update(
      req.body,
      {where: {id:req.params.id}}
    )
    .then(function(rowsUpdated) {
      res.json(rowsUpdated)
})
.catch((error) => res.status(500).send({ error }))

});

module.exports = router;
