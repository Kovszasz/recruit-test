// Returns the error to the client
module.exports = (app) => {
  app.use((err, req, res, next) => {
    console.error(err.message);
    res.status(err.status || 500).send({
      message: err.message,
      error: err
    });
  });
};
